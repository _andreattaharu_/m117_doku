### Lernjournal - 26. Mai 2023

Heute habe ich viel über Netzwerktechnologie gelernt. Wir haben über verschiedene Datenübertragungsarten wie Simplex, Half-Duplex und Duplex gesprochen. Außerdem haben wir die Topologien Bus, Ring, Stern, Baum und Mesh behandelt. Wir haben auch die Übertragungsmedien Draht, Litze und Glas diskutiert und gelernt, wie man Störeinflüsse abwehren kann, zum Beispiel durch Abschirmung und TwistedPair-Verkabelung. Es gab auch Informationen über Kabelkategorien wie CAT7 und Ethernet-Medientypen wie 1000Base-T. Schließlich haben wir über die universelle Gebäudeverkabelung (UGV) gesprochen, bei der Draht für die Verbindungen verwendet wird. Ich freue mich darauf, dieses Wissen praktisch anzuwenden und mehr über dieses interessante Thema zu erfahren.

Der erste Tag des Moduls hat mir gut gefallen. Die behandeln Inhalte waren interessant und gut strukturiert. Ich habe einen guten Überblick über die Grundlagen der Netzwerktechnologie bekommen. Ich freue mich auf die kommenden Tage und darauf, mein Wissen weiter auszubauen. Insgesamt war es ein gelungener Start. 


### Lernjournal - 2. Juni 2023

Heute hatten wir einen entspannten Tag im Modul Netzwerktechnologie. Wir haben verschiedene Themen behandelt und unser Wissen erweitert.

Im ersten Teil des Tages haben wir offene Fragen geklärt und ein Kahoot-Quiz gemacht, um unser Verständnis zu überprüfen.

Im zweiten Teil haben wir uns mit Verkabelung befasst. Wir haben verschiedene Kabelarten besprochen und einen Verkabelungsplan erstellt.

Im dritten Teil ging es um den Internetzugang über ISPs. Wir haben gelernt, wie ISPs Verbindungen herstellen und welche Technologien sie verwenden.

Im vierten Teil haben wir WLAN behandelt. Wir haben Normen, Antennen, Sicherheitsmaßnahmen und WLAN-Abdeckung besprochen.

Es war ein gelungener Tag, und ich freue mich darauf, mein Wissen weiter auszubauen.

### Lernjournal - 9. Juni 2023
Heute haben wir uns intensiv mit der Netzwerkadressierung beschäftigt und verschiedene Konzepte behandelt:

- Wiederholung der IP-Adressierung und Unterschiede zwischen IPv4 und IPv6.
- Subnetzmasken und Subnetting zur Bestimmung von Netz-ID und Host-ID.
- Berechnung der Anzahl verfügbarer IP-Adressen und Hosts in einem Subnetz.
- Bedeutung der Netzwerkadresse und der Broadcastadresse.

Es war ein lehrreicher Tag, und ich freue mich darauf, mein Wissen in der Praxis anzuwenden.

### Lernjournal - 16. Juni 2023

Heute hatten wir einen lehrreichen Tag in der TBZ. Wir haben über IP-Adressen gesprochen und gelernt, dass es private Adressbereiche gibt, die für interne Netzwerke reserviert sind. Diese Bereiche sind 10.0.0.0 bis 10.255.255.255, 172.16.0.0 bis 172.31.255.255 und 192.168.0.0 bis 192.168.255.255.

Wir haben auch das lokale Netzwerk (LAN) in der TBZ erkundet, speziell das 10-er Netz. Dabei haben wir erfahren, wie die Geräte im Netzwerk miteinander kommunizieren und welche IP-Adressen dafür verwendet werden.

Das Highlight des Tages war die praktische Arbeit mit dem CISCO Packet-Tracer. Wir konnten virtuelle Geräte verwenden und damit ein Netzwerk aufbauen. Es war spannend, verschiedene Konfigurationen auszuprobieren und zu sehen, wie sie sich auswirken. Das hat uns geholfen, unsere Kenntnisse anzuwenden und praktische Fähigkeiten zu entwickeln.

Insgesamt war der Tag produktiv und lehrreich. Ich freue mich darauf, in den kommenden Lektionen noch mehr über IP-Adressen und Netzwerke zu lernen und meine Fähigkeiten weiter zu verbessern.

### Lernjournal - 23. Juni 2023

Heute haben wir im Unterricht Teil 3 und Teil 5 der Aufgabenblätter zu IP bearbeitet und die entsprechenden Praxisübungen im PacketTracer durchgeführt. Es war eine herausfordernde, aber lohnenswerte Aufgabe.

In der Prüfungsbesprechung haben wir Informationen über Hubs und Switches erhalten. Im Anschluss erfolgte eine Einführung in das ISO-OSI-Modell. Wir haben den Sinn und Zweck dieses Modells verstanden und gelernt, wie MAC-Adressen, IP-Adressen und Portnummern den verschiedenen Schichten des OSI-Modells zugeordnet sind. Diese Kenntnisse sind wichtig, um zu verstehen, wie Datenpakete im Netzwerk verschickt werden.

Abschließend haben wir uns detailliert mit dem Versenden von Paketen beschäftigt. Wir haben die verschiedenen Schritte und Prozesse durchgegangen, die beim Versenden eines Pakets stattfinden, und dabei die einzelnen Schichten des OSI-Modells berücksichtigt.

Wir haben uns einen tieferen Einblick in die Funktionsweise von Hubs, Switches und dem ISO-OSI-Modell gegeben. Die praktischen Übungen im PacketTracer haben uns dabei geholfen, das Gelernte anzuwenden und zu vertiefen. Ich freue mich darauf, weiterhin mein Verständnis für Netzwerke und Datenübertragung zu erweitern.

### Lernjournal - 30. Juni 2023

Lernjournal Eintrag - 10. Juli 2023

Heute fand eine Fragerunde statt, in der wir die Gelegenheit hatten, offene Fragen zu stellen und etwaige Unklarheiten zu beseitigen. Es war eine gute Möglichkeit, unser Wissen zu festigen und eventuelle Lücken zu schließen.

Im Anschluss haben wir das OSI-Modell wiederholt. Wir haben uns den Weg eines Ethernet-Frames bei der Querung eines Switches und eines Routers genauer angesehen. Dies half uns, das Verständnis für die Datenübertragung in verschiedenen Netzwerkelementen zu vertiefen.

Wir haben auch die TCP/IP-Arbeiten abgeschlossen. Dies beinhaltete möglicherweise die Durchführung von Praxisübungen und das Überprüfen unserer Kenntnisse in Bezug auf das TCP/IP-Protokoll.

Zwischendurch gab es Feedback zu ausgewählten ePortfolios. Es war wertvoll, Rückmeldungen zu erhalten und zu sehen, wie wir unsere Theorie und praktischen Arbeiten verbessern können.

Anschließend erhielten wir eine kurze Einführung in das Thema Benutzer und Berechtigungen sowie das NTFS-Dateisystem. Dies war eine Vorbereitung auf das Selbststudium der abgegebenen Theorie und das Lösen der zugehörigen Aufgaben. Wir wurden darauf hingewiesen, dass wir weitere Informationen und Aufgaben im Kanal "Allgemein" finden können.

Wir haben am Ende die LB2 gemacht


### Lernjournal - 7. Juli 2023

Heute fand die Prüfungsbesprechung zur LB2 statt. Wir hatten die Gelegenheit, offene Fragen zu klären und das Feedback zu unserer Leistung zu erhalten. Es war eine wichtige Möglichkeit, unser Verständnis zu vertiefen und uns auf mögliche Verbesserungen zu konzentrieren.

Im Anschluss daran hatten wir eine Fragerunde, in der wir weitere Fragen stellen konnten. Dies gab uns die Möglichkeit, eventuelle Unklarheiten zu beseitigen und sicherzustellen, dass wir alle Themen gut verstanden haben.

Es folgte ein kurzer Flashback zum Tag 6, um die Erinnerungen an das bisher Gelernte aufzufrischen. Dies half uns dabei, den Zusammenhang zwischen den verschiedenen Konzepten und Aufgaben herzustellen.

Ein weiterer Schwerpunkt lag auf einer Demo an zwei virtuellen PCs. Wir haben gelernt, wie man ein virtuelles Netzwerk mit Hilfe von "ncpa.cpl" einrichtet und überprüft. Dabei haben wir auch darauf geachtet, dass eventuell vorhandene Firewalls berücksichtigt werden. Des Weiteren haben wir Benutzer, Gruppen und Freigaben eingerichtet und überprüft. Es war auch wichtig, den Diskspace richtig einzurichten und beispielsweise Arbeitsverzeichnisse von Systemverzeichnissen zu trennen.

Abschließend haben wir die Arbeitsblätter, die wir am Tag 6 im Kanal "Allgemein" abgegeben haben, fertiggestellt. Dies ermöglichte uns, unsere Theorie und praktischen Fähigkeiten weiter zu vertiefen und sicherzustellen, dass wir die gestellten Aufgaben vollständig abgeschlossen haben.
