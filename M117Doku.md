# M117_Doku

## 26/05/2023
### Teil 1 Netzwerkverbinding

#### Wie funktioniert das heutige LAN?

Das lokale Netzwerk (Local Area Network, LAN) ist eine Technologie, die es ermöglicht, Geräte in einem begrenzten geografischen Bereich miteinander zu verbinden. Es gibt verschiedene Technologien und Standards, die für ein LAN verwendet werden können, aber ich werde die allgemeinen Prinzipien und Komponenten eines heutigen LANs beschreiben.

1. Netzwerktopologie: Ein LAN kann verschiedene Topologien verwenden, wie beispielsweise Bus, Ring oder Stern. Die häufigste Topologie für moderne LANs ist jedoch die Stern-Topologie. Bei dieser Topologie sind alle Geräte mit einem zentralen Netzwerk-Switch verbunden.

2. Netzwerk-Switch: Der Netzwerk-Switch ist das Herzstück eines LANs. Er ermöglicht die Verbindung und Kommunikation zwischen den Geräten im Netzwerk. Moderne Switches sind in der Regel Layer-2- oder Layer-3-Switches, die den Datenverkehr anhand von MAC-Adressen oder IP-Adressen weiterleiten.

3. Verkabelung: Die Verkabelung spielt eine wichtige Rolle bei der Bereitstellung eines zuverlässigen LANs. Heutzutage wird häufig Ethernet-Kabel (z. B. Cat 5e, Cat 6 oder Cat 6a) verwendet, um die Geräte mit dem Netzwerk-Switch zu verbinden.

4. Netzwerkprotokolle: LANs verwenden verschiedene Netzwerkprotokolle, um die Kommunikation zwischen den Geräten zu ermöglichen. Das am weitesten verbreitete Protokoll ist das TCP/IP-Protokoll, das das Internet und viele LANs antreibt.

5. IP-Adressierung: Jedes Gerät in einem LAN erhält eine eindeutige IP-Adresse, die es identifiziert und die Kommunikation ermöglicht. Für IPv4-Netzwerke besteht eine typische LAN-IP-Adresse aus vier Zahlenblöcken (z. B. 192.168.0.1), während für IPv6 längere Adressen verwendet werden.

6. Netzwerkdienste: LANs bieten verschiedene Netzwerkdienste wie Dateifreigabe, Drucken, E-Mail und Internetzugang. Diese Dienste werden häufig durch dedizierte Server bereitgestellt, die im LAN gehostet werden.

7. Netzwerksicherheit: LANs implementieren Sicherheitsmaßnahmen wie Firewalls, Intrusion Detection/Prevention-Systeme und Zugriffskontrolllisten, um das Netzwerk vor unbefugtem Zugriff und Bedrohungen zu schützen.

8. Drahtlose LANs: Neben kabelgebundenen Verbindungen werden auch drahtlose LANs (WLANs) immer beliebter. WLANs nutzen drahtlose Technologien wie Wi-Fi, um Geräte drahtlos mit dem Netzwerk zu verbinden. Die meisten modernen LANs integrieren sowohl drahtgebundene als auch drahtlose Verbindungen.

#### Netzwerktopologien

1. Bus Topologie
2. Stern Topologie
3. Ring Topologie
4. Baum Topologie 
5. Mashen Topoligie

#### Draht, Litze und Glas

Draht (Kupfer):
- Vorteile: Kostengünstig, einfach zu installieren.
- Nachteile: Geringere Bandbreite, anfällig für Störungen.
- Verwendung: Ethernet-Verbindungen in Büros und Wohnhäusern.

Litze (flexibles Kupferkabel):
- Vorteile: Flexibel, leicht zu biegen.
- Nachteile: Anfällig für Brüche, Signalverluste bei höheren Frequenzen.
- Verwendung: Patchkabel für Geräteverbindungen.

Glasfaser:
- Vorteile: Hohe Bandbreite, immun gegen Störungen, sicher.
- Nachteile: Teurer, erfordert spezielles Fachwissen.
- Verwendung: Langstrecken-Backbone-Verbindungen, Telekommunikationsnetze.

#### Kabel-Abschirmung

1. Unschielded, Twisted-Pair
2. Shielded, Twisted-Pair
3. Unschielded, Screened, Twisted-Pair
4. Shielded, Foiled, Screened, Twisted-Pair
5. Shielded, Foiled, Screened, Twisted-Pair
6. Shielded, Foiled, Twisted-Pair
7. Shielded, FOiled, Screened, Twisted-Pair
8. Shielded, Twisted-Pair

#### Ethernet Medien-typen

1. 1000Base-LX:
   - Material: Glasfaser/Fibre
   - Verbindungsart: Stern/Duplex
   - IEEE-Norm: 802.3z (Jahr 1998)
   - Datendurchsatz Brutto: 1 Gb/s (Gigabit!)
   - Technologie: Long-Wavelength (1310nm)
   - Segmentlänge max.: Singlemode: 5 km; Multimode: 550m

2. 10GBase-T:
   - Material: Kupferverbindung
   - Verbindungsart: Stern/Duplex
   - IEEE-Norm: 802.3an (Jahr 2006)
   - Datendurchsatz Brutto: 10 Gb/s (10 Gigabit!)
   - Kabelbelegung: 4x100Ω Aderpaare
   - Kabelkategorie mindestens: CAT6a
   - Segmentlänge max.: 55m (CAT6a), 100m (CAT7)

3. 10GBase-SR:
   - Material: Glasfaser/Fibre
   - Verbindungsart: Stern/Duplex
   - IEEE-Norm: 802.3ae (Jahr 2002)
   - Datendurchsatz Brutto: 10 Gb/s (10 Gigabit!)
   - Technologie: Short-Reach (850nm)
   - Segmentlänge max.: Multimode: 26m (OM1), 82m (OM2), 300m (OM3), 400m (OM4)

4. 10GBase-LR:
   - Material: Glasfaser/Fibre
   - Verbindungsart: Stern/Duplex
   - IEEE-Norm: 802.3ae (Jahr 2002)
   - Datendurchsatz Brutto: 10 Gb/s (10 Gigabit!)
   - Technologie: Long-Reach (1310nm)
   - Segmentlänge max.: Singlemode: 10 km
  
5. 1000Base-SX:
   - Material: Glasfaser
   - Verbindungsart: Stern/Duplex
   - IEEE-Norm: 802.3z (1998)
   - Datenrate: 1 Gbit/s
   - Wellenlänge: 850nm
   - Segmentlänge max.: Multimode: 550m
  --------

## 02/06/2023
### Teil2 Der Zugang ins Internet

#### ISP-Auftrag

Für den Wohnort in Zürich werde ich zwei ISP-Angebote evaluieren und die Ergebnisse in einer tabellarischen Aufstellung präsentieren. 

| ISP        | Technologie   | Datendurchsatzrate     | Preis         | Router   | Einmalige Gebühren | Support                        | Vertragsdauer | Netz-Verfügbarkeit | Skalierbarkeit |
|------------|---------------|------------------------|---------------|----------|--------------------|--------------------------------|----------------|--------------------|----------------|
| Yallo      | VDSL          | 100 Mbps (Download)     | CHF 50/Monat  | Ja       | CHF 0              | Hotline: 24/7, Vor-Ort-Service: Ja | 12 Monate      | 99,9%             | Ja             |
| Sunrise    | Kabelinternet | 250 Mbps (Download)     | CHF 60/Monat  | Ja       | CHF 30             | Hotline: 8-20 Uhr, Vor-Ort-Service: Ja | 24 Monate      | 99,5%             | Ja             |

### Die Netzwerkadressierung

#### Aufgabe 5 

##### Fall 1

IP-Adresse: 10.35.3.112
Subnetzmaske: 255.0.0.0 / 8
Netz-ID: 10.0.0.0
Host-ID: 35.3.112
Anzahl IP-Adressen im Subnetz: 16.777.216
Anzahl Hosts im Subnetz: 16.777.214
Netzwerkadresse: 10.0.0.0
Broadcastadresse: 10.255.255.255

##### Fall 2

IP-Adresse: 172.16.43.87
Subnetzmaske: 255.255.0.0 / 16
Netz-ID: 172.16.0.0
Host-ID: 43.87
Anzahl IP-Adressen im Subnetz: 65.536
Anzahl Hosts im Subnetz: 65.534
Netzwerkadresse: 172.16.0.0
Broadcastadresse: 172.16.255.255

##### Fall 3

IP-Adresse: 192.168.17.37
Subnetzmaske: 255.255.255.0 / 8
Netz-ID: 192.0.0.0
Host-ID: 168.17.37
Anzahl IP-Adressen im Subnetz: 16.777.216
Anzahl Hosts im Subnetz: 16.777.214
Netzwerkadresse: 192.0.0.0
Broadcastadresse: 192.255.255.255
#### Aufgabe 11

##### 1

Für die IP-Adresse 192.168.3.37/24 ist die Netz-ID 192.168.3.0 und die Host-ID 0.37. Dies ergibt sich aus der Subnetzmaske /24, die die ersten 24 Bits für die Netz-ID reserviert und die letzten 8 Bits für die Host-ID.

##### 2

Für die IP-Adresse 78.23.49.123/255.255.255.0 ist die Netzwerkadresse 78.23.49.0 und die Broadcastadresse 78.23.49.255. Dies ergibt sich aus der Subnetzmaske 255.255.255.0, die die ersten 24 Bits für die Netzwerkadresse reserviert und die letzten 8 Bits für den Broadcast verwendet.

##### 3

Die IP-Adresse 78.256.125.12 ist ungültig, weil die Werte für die Oktetten (Teile) der IP-Adresse im Bereich von 0 bis 255 liegen müssen. Der Wert "256" ist außerhalb dieses Bereichs und daher nicht zulässig.

Die Subnetzmaske 255.255.248.0 ist gültig, da sie eine korrekte Darstellung einer IPv4-Subnetzmaske ist. Sie gibt an, dass die ersten 21 Bits der IP-Adresse für die Netz-ID verwendet werden und die verbleibenden 11 Bits für die Host-ID reserviert sind. Dies bedeutet, dass es sich um ein Subnetz mit 2^11 = 2048 möglichen Hosts handelt. Allerdings kann diese Subnetzmaske nicht auf die ungültige IP-Adresse 78.256.125.12 angewendet werden, da sie außerhalb des gültigen Adressbereichs liegt. 

##### 4 

Ja, es ist möglich, einem PC die IP-Adresse 172.30.0.0 zuzuweisen, solange diese Adresse im Subnetz des PCs liegt und nicht bereits von einem anderen Gerät im Netzwerk verwendet wird.

##### 5

Die gegebenen Netzwerkeinstellungen weisen darauf hin, dass der PC eine IP-Adresse von 10.23.65.128 mit einer Subnetzmaske von 16 und ein Standardgateway von 10.24.0.1 hat. Diese Konfiguration ist falsch, da die IP-Adresse und die Subnetzmaske nicht zusammenpassen. 

##### 6

Die Adresse 10.255.255.254 und 172.25.123.4 sind beide im Privaten Bereich. 

##### 7

 Die IP-Adresse 169.254.0.1 mit der Subnetzmaske 16 deutet auf eine APIPA hin. Diese Adresse wird verwendet, wenn ein Gerät keine gültige IP-Adresse von einem DHCP-Server beziehen kann.

##### 8

 Die IP-Adresse 127.0.0.1 ist die Loopback-Adresse und wird immer auf den lokalen Host verweisen. Es wird verwendet, um auf den eigenen Computer zuzugreifen. Wenn Sie diese Adresse in einem Webbrowser oder einem anderen Netzwerktool eingeben, wird der Datenverkehr direkt an die Netzwerkkarte des eigenen Computers gesendet, ohne das Netzwerk zu verlassen.

##### 9 

Die beiden PCs mit den IP-Adressen 172.16.3.48/24 und 172.16.4.126/24 befinden sich nicht im gleichen Netzwerksegment mit der Subnetzmaske /24. Da ihre IP-Adressen im Bereich von 172.16.3.1 bis 172.16.3.254 und 172.16.4.1 bis 172.16.4.254 liegen, befinden sie sich nicht im selben Subnetz.

##### 10  

Die Adresse 172.22.17.201 mit der Subnetzmaske 255.0.0.0 hat ein 8-Bit-Netzwerkpräfix, was bedeutet, dass die ersten 8 Bits der IP-Adresse das Netzwerkidentifikationspräfix darstellen. Dieses Präfix gibt an, dass alle IP-Adressen, die mit "172." beginnen, im selben Netzwerk liegen. Mit der Subnetzmaske 255.0.0.0 können jedoch alle IP-Adressen zwischen 172.0.0.0 und 172.255.255.255 erreicht werden. Dieses Subnetz ist sehr groß und enthält viele potenzielle IP-Adressen, sodass es für kleinere Netzwerke möglicherweise zu groß ist und zu Verschwendung von IP-Adressen führt.

##### 11

Ein Switch analysiert normalerweise die MAC-Adresse eines eingehenden Pakets, um festzustellen, an welchem Port er das Paket weiterleiten muss. Ein Router hingegen analysiert die IP-Adresse und MAC-Adresse eines Pakets, um festzustellen, an welches Netzwerk oder welchen Host er das Paket weiterleiten muss. 

###### 12 

Das Problem besteht darin, dass die MAC-Adresse eine eindeutige Kennung für Netzwerkgeräte ist. Wenn Ihr Hackerfreund die MAC-Adresse seiner Netzwerkkarte auf Ihre MAC-Adresse ändert und beide Geräte gleichzeitig im Netzwerk aktiv sind, kommt es zu einer Konflikt- und Duplikationssituation.

## 16/06/2023

### NetzwerkAddressierung

#### 1 
![Aufgabe 1](src/Aufgabe5.1.png)

#### 2
![Aufgabe 2](src/Aufgabe5.2.png)

#### 3
![Aufgabe 3](src/Aufgabe5.3.png)

